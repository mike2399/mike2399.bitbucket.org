
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";
$("#pData").hide();


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}
//zacetek!
$.ajaxSetup({
    headers: {
        "Authorization": getAuthorization()
    }
});

var pacienti = [{
        "firstNames": "Marko",
        "lastNames": "Jarec",
        "gender": "MALE",
        "dateOfBirth": "1989-05-22",
        "address": {
            "address": "Ljubljana, Slovenija"
        },
        "additionalInfo": {
            "ehrId" : "",
            "graph1" : "67,66,64,65",
            "height" : 168.8,
            "weight" : 65,
            "systolic": 125,
            "diastolic": 90,
            "mobile": 040888888,
            "facebook": "Marko Jarec",
            "gmail": "marko.jarec@gmail.com",
            "temperature": "37.5 C"
        }
        //    {"key": "visina", "value": 168.8},
         //   {"key": "temperatura", "value": 168.8},
         //   {"key": "teza", "value": 168.8},
         //   {"key": "tlak_dia", "value": 168.8},
         //   {"key": "tlak_sis", "value": 168.8},
         //   {"key": "o2", "value": 168.8}
       // }
    },
    {
        "firstNames": "Miha",
        "lastNames": "Novak",
        "gender": "MALE",
        "dateOfBirth": "1999-09-23",
        "address": {
            "address": "Grosuplje, Slovenija"
        },
        "additionalInfo": {
            "ehrId" : "",
            "graph1" : "73,74,76,74",
            "height" : 182.5,
            "weight" : 74,
            "systolic": 118,
            "diastolic": 85,
            "mobile": 040758203,
            "facebook": "Miha Novak",
            "gmail": "miha.novak@gmail.com",
            "temperature": "36.8 C" 
        }
    },
     {
        "firstNames": "Aleš",
        "lastNames": "Česen",
        "gender": "MALE",
        "dateOfBirth": "1978-08-06",
        "address": {
            "address": "Maribor, Slovenija"
        },
        "additionalInfo": {
            "ehrId" : "",
            "graph1" : "95,96,95,98",
            "height" : 168.8,
            "weight" : 98,
            "systolic": 110,
            "diastolic": 75,
            "mobile": 031594076,
            "facebook": "Aleš Česen",
            "gmail": "ales.cesen@gmail.com",
            "temperature": "36.2 C"
        }
    },
];
function racunajBMI(visina, teza) {
    $("#bmiTestek").removeClass();
    var rezultat;
    var rezultat3;
    var visina2 = parseFloat(visina);
    visina2 = visina2/100;
    var teza2 = parseFloat(teza);
    
    rezultat = teza2/(visina2*visina2);
    rezultat = rezultat.toFixed(1);
    rezultat3 = rezultat.toString();
    $("#bmiVal").html(rezultat3);
    if (rezultat > 25) {
        $("#bmiTestek").addClass("fas fa-exclamation-triangle");
        $("#bmiTestek").css("color", "red");
    }
    else if (rezultat > 18.5 && rezultat < 25) {
        $("#bmiTestek").addClass("far fa-thumbs-up");
        $("#bmiTestek").css("color", "green");
    }
    else {
        $("#bmiTestek").addClass("fas fa-exclamation-triangle");
        $("#bmiTestek").css("color", "orange"); 
    }
    console.log(rezultat);
}

function preveriTemperaturo(temp) {
    $("#tempTestek").removeClass();
    var temp2 = parseFloat(temp);
    
    if (temp2 >= 37) {
        $("#tempTestek").addClass("fab fa-hotjar");
        $("#tempTestek").css("color", "red");
    }
    else if (temp2 >= 36.5 && temp2 < 37) {
        $("#tempTestek").addClass("far fa-smile");
        $("#tempTestek").css("color", "green");  
    }
    else {
        $("#tempTestek").addClass("far fa-snowflake");
        $("#tempTestek").css("color", "blue"); 
    }
}

function zgornjiPritisk(pritisk) {
    var pritisk2 = parseFloat(pritisk);
    $("#sisTestek").removeClass();
    if (pritisk2 <= 120) {
        $("#sisTestek").addClass("fas fa-heartbeat");
        $("#sisTestek").css("color", "green");  
    }
    else {
        $("#sisTestek").addClass("fas fa-heartbeat");
        $("#sisTestek").css("color", "red");  
    }
}

function spodnjiPritisk(pritisk) {
    var pritisk2 = parseFloat(pritisk);
    $("#diaTestek").removeClass();
    if (pritisk2 <= 80) {
        $("#diaTestek").addClass("fas fa-heartbeat");
        $("#diaTestek").css("color", "green");  
    }
    else {
        $("#diaTestek").addClass("fas fa-heartbeat");
        $("#diaTestek").css("color", "red");  
    }
}
function pacientSelected(i){
    var p = pacienti[i];
    
    $("#patient-name").html(`${p.firstNames} ${p.lastNames}`);
    $("#patient-age").html(`23`);

    //$("#result").html(JSON.stringify(pacienti[i]));
    $("#persVal").html(p.firstNames+" "+p.lastNames);
    $("#genderVal").html(p.gender);
    $("#firstVal").html(p.firstNames);
    $("#lastVal").html(p.lastNames);
    $("#birthVal").html(p.dateOfBirth);
    $("#localVal").html(p.address.address);
    $("#hPodnaslov").html(p.firstNames+" "+p.lastNames);
    $("#mobileVal").html(p.additionalInfo.mobile);
    $("#fabVal").html(p.additionalInfo.facebook);
    $("#gmailVal").html(p.additionalInfo.gmail);
    $("#heightVal").html(p.additionalInfo.height+" cm");
    $("#weightVal").html(p.additionalInfo.weight+" kg");
    $("#sisVal").html(p.additionalInfo.systolic);
    $("#diaVal").html(p.additionalInfo.diastolic);
    $("#tempval").html(p.additionalInfo.temperature);
    racunajBMI(p.additionalInfo.height, p.additionalInfo.weight);
    preveriTemperaturo(p.additionalInfo.temperature);
    zgornjiPritisk(p.additionalInfo.systolic);
    spodnjiPritisk(p.additionalInfo.diastolic);
    updateGraph(p);
}

//konec!

window.addEventListener('load', function() {
 // document.getElementById("sidebar").addEventListener("click", function() {
//          document.getElementById("sidebar").classList.toggle('active');
  //});
  $("#pData").fadeOut(0);
  $("#hData").fadeOut(0);
  $("#doctors").fadeOut(0);
  $("#result").fadeOut(0);
});

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
    var stevec = 0;
    var stevec2 = 0;
     $("#obvestilo").empty();
    
  pacienti.forEach(function(pacient, i){
        generirajPacienta(pacient, function(noviPacient){
            pacienti[i] = noviPacient;
            console.log(pacienti[i].additionalInfo.ehrId);
            $("#obvestilo").append("<span class='obvestilo " +
                "label label-success fade-in'>Uspešno kreiran EHR '" +
                pacienti[i].additionalInfo.ehrId + "'.</span><br>");
            
              $("#preberiEHRid").val(ehrId);
            $("#pacienti").append(`<p>${JSON.stringify(noviPacient)}</p>`);
            $("#pacientDropDownMenu").append(`
                <a id="pacient-${i}" onclick="pacientSelected(${i})" class="dropdown-item" href="#!">${noviPacient.firstNames} ${noviPacient.lastNames}</a>
                <br>
            `);
            
        });

    });
        $("#obvestilo").fadeIn(0);
        $("#obvestilo").fadeOut(4000);
  ehrId = "";

  // TODO: Potrebno implementirati

  return ehrId;
}
function updateGraph(person){
   person.additionalInfo.graph1.split(',').forEach(function(str,i){
        charts[0].data[0].dataPoints[i] = {
            'label': `Teža:`,
            'y': parseFloat(str)
        };
    });
       charts.forEach(function(c){c.render()});
}
function onCheckBoxClick(){
    $('#customCheck1').prop('checked', false);
}

var charts = [];

    window.onload = function() {
        try {
            charts.push(new CanvasJS.Chart("chartContainer1", {
                title: { text: "Graf teže uporabnika"},
                data: [
                    { dataPoints: [{}] }
                ]
            }));
            charts.forEach(function(c){c.render()});
        }
        catch(err) {
            console.log(err);
        }
    };

function generirajPacienta(pacientInfo, cb) {

    $.ajax({
        url: baseUrl + "/ehr",
        type: 'POST',
        headers: {
            "Authorization": getAuthorization()
        },
        
        success: function(data) {
            var ehrId = data.ehrId;
            console.log(pacientInfo);
            pacientInfo.additionalInfo.ehrId = ehrId;

            // build party data
            $.ajax({
                url: baseUrl + "/demographics/party",
                type: 'POST',
                contentType: 'application/json',
                headers: {
                    "Authorization": getAuthorization()
                },
                data: JSON.stringify(pacientInfo),
                success: function(party) {
                    if (party.action == 'CREATE') {
                        cb(pacientInfo);
                    }
                }
            });
        }
    });
}

function searchPacient(){
    var ehrId = $("#pacientEHR").val();

    var searchData = [
        {key: "ehrId", value: ehrId}
    ];
    
    $("#persVal").html("-");
    $("#genderVal").html("-");
    $("#firstVal").html("-");
    $("#lastVal").html("-");
    $("#birthVal").html("-");
    $("#localVal").html("-");
    $("#hPodnaslov").html("-");
    $("#mobileVal").html("-");
    $("#fabVal").html("-");
    $("#gmailVal").html("-");
    $("#heightVal").html("-");
    $("#weightVal").html("-");
    $("#sisVal").html("-");
    $("#diaVal").html("-");
    $("#tempval").html("-");
    $("#bmiVal").html("-");

    $.ajax({
        url: baseUrl + "/demographics/party/query",
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(searchData),
        success: function (res) {
            for (i in res.parties) {
                var party = res.parties[i];
                try {
                    $("#result").html(`<p>${JSON.stringify(party)}</p>`);
                }
                catch(err) {
                    $("#result").html("-");
                }
                try {
                     $("#persVal").html(party.firstNames +" "+ party.lastNames);
                }
                catch(err) {
                    $("#persVal").html("-");
                }
                try {
                    $("#hPodnaslov").html(party.firstNames +" "+ party.lastNames);
                }
                catch(err) {
                    $("#hPodnaslov").html("-");
                }
                try {
                    $("#firstVal").html(party.firstNames);
                }
                catch(err) {
                    $("#firstVal").html("-");
                }
                try {
                     $("#lastVal").html(party.lastNames);
                }
                catch(err) {
                    $("#lastVal").html("-");
                }
                try {
                     $("#genderVal").html(party.gender);
                }
                catch(err) {
                    $("#genderVal").html("-");
                }
                try {
                     $("#birthVal").html(party.dateOfBirth);
                }
                catch(err) {
                    $("#birthVal").html("-");
                }
                //if (party.address)
                //$("#localVal").html(party.address.address);
                
                //console.log(res.parties[i]);
                
                try {
                 $("#localVal").html(party.address.address);
                }
                catch(err) {
                   $("#localVal").html("-");
                }
                  try {
                 $("#mobileVal").html(party.additionalInfo.mobile);
                }
                catch(err) {
                   $("#mobileVal").html("-");
                }
                  try {
                 $("#fabVal").html(party.additionalInfo.facebook);
                }
                catch(err) {
                   $("#fabVal").html("-");
                }
                  try {
                 $("#gmailVal").html(party.additionalInfo.gmail);
                }
                catch(err) {
                   $("#gmailVal").html("-");
                }
                  try {
                 $("#heightVal").html(party.additionalInfo.height);
                }
                catch(err) {
                   $("#heightVal").html("-");
                }
                  try {
                 $("#weightVal").html(party.additionalInfo.weight);
                }
                catch(err) {
                   $("#weightVal").html("-");
                }
                  try {
                 $("#sisVal").html(party.additionalInfo.systolic);
                }
                catch(err) {
                   $("#sisVal").html("-");
                }
                  try {
                 $("#diaVal").html(party.additionalInfo.diastolic);
                }
                catch(err) {
                   $("#diaVal").html("-");
                }
                  try {
                 $("#tempval").html(party.additionalInfo.temperature);
                }
                catch(err) {
                   $("#tempval").html("-");
                }
                $("#bmiTestek").removeClass();
                $("#tempTestek").removeClass();
                $("#sisTestek").removeClass();
                $("#diaTestek").removeClass();
                if ($("#heightVal").html() != "-" && $("#weightVal").html() != "-") {
                    racunajBMI(party.additionalInfo.height, party.additionalInfo.weight);
                }
                if ($("#tempval").html() != "-") {
                    preveriTemperaturo(party.additionalInfo.temperature);
                }
                if ($("#sisVal").html() != "-") {
                    zgornjiPritisk(party.additionalInfo.systolic);
                }
                if ($("#diaVal").html() != "-") {
                    spodnjiPritisk(party.additionalInfo.diastolic);
                }
            }
        }
    });
}

function showIndex(parameter) {
  switch(parameter) {
    case "home": {
      $("#pData").fadeOut(0);
      $("#result").fadeOut(0);
      $("#doctors").fadeOut(0);
      $("#hData").fadeOut(0);
      $("#home").fadeIn(0);
      break;
    }
    case "pData": {
      $("#home").fadeOut(0);
      $("#result").fadeOut(0);
      $("#doctors").fadeOut(0);
      $("#hData").fadeOut(0);
      $("#pData").fadeIn(0);
      break;
    }
    case "hData": {
      $("#home").fadeOut(0);
      $("#result").fadeOut(0);
      $("#doctors").fadeOut(0);
      $("#pData").fadeOut(0);
      $("#hData").fadeIn(0);
      break;
    }
    case "doctors": {
      document.getElementById("hopsasa").innerHTML = ("");
      $("#home").fadeOut(0);
      $("#result").fadeOut(0);
      $("#pData").fadeOut(0);
      $("#hData").fadeOut(0);
      $("#doctors").fadeIn(0);
      break;
    }
    //case "result": {
      //$("#home").fadeOut(0);
      //$("#pData").fadeOut(0);
      //$("#hData").fadeOut(0);
      //$("#doctors").fadeOut(0);
      //$("#result").fadeIn(0);
      //break;
    //}
  console.log("izvedel sem se!");
  }
}

  var dropdown = $('#locality-dropdown');

    dropdown.empty();

    dropdown.append('<option selected="true" disabled>Choose State/Province</option>');
    dropdown.prop('selectedIndex', 0);
    
    var url = 'knjiznice/json/data.json';
    
    // Populate dropdown with list of provinces
    var json;
 function pridobiBMI(callback) {
    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open("GET", "knjiznice/json/data.json", true);
    xobj.onreadystatechange = function () {
      // rezultat ob uspešno prebrani datoteki
        if (xobj.readyState == 4 && xobj.status == "200") {
           json = JSON.parse(xobj.responseText);
          //console.log(json.dims.COUNTRY);
          var i = 0;
           $.each(json.fact, function (key, entry) {
               console.log(json.fact[i].dims.COUNTRY);
               console.log(entry);
        var value = json.fact[i].dims.COUNTRY;
        var name = json.fact[i].dims.COUNTRY;
        $("#locality-dropdown").append(`<option class="izbira">${value}</option>`);
               i++;
           });
        
           
          // var nov = document.createElement('option');
          // nov.innerHTML = $("#nov_element").val(json.fact[i].dims.COUNTRY);
          
         // var option;
          //  for (let i = 0; i < json.length; i++) {
        //      option = document.createElement('option');
          ////  option.value = json[i].dims.COUNTRY;
              //dropdown.add(option);
            //}
            // nastavimo ustrezna polja (število najdenih zadetkov)
           // vrnemo rezultat
          callback(json);
          
        //console.log(json);
      } 
    };
    xobj.send(null);
}
    
    function izpisiTekst() {
      var x = document.getElementById("locality-dropdown").selectedIndex;
      var y = document.getElementById("locality-dropdown").options;
      //alert("Index: " + y[x].index + " is " + y[x].text);
      //json.fact[y[x].index].Value
      
      console.log(json.fact[y[x].index].Value);
      document.getElementById("hopsasa").innerHTML = ("Percentage of people in "+y[x].text+" that are overweight is (percentage and mediana): "+json.fact[y[x].index].Value);
    }
    
    function dodajPodatke(){
    //    var teza = 0.0;
    //    var stvari = jsonRezultat.fact;
        
    //    for (var i = 0; i < stvari.length; i++) {
            
    //    }
    //    var i = 0;
    
    
      pridobiBMI(function (jsonRezultat){
       // console.log(jsonRezultat.fact["0"].dims.COUNTRY);
      });
      
    }
    dodajPodatke();
    
    



// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
